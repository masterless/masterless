---
title: "Free Software is dead"
date: 2020-11-15T16:30:42Z
draft: false
---

>For me, the best parts of the open-source movement were always the remnants of the “free software movement” from which it evolved. During the early days of the movement in the 1980s, best captured by Richard Stallman’s book Free Software, Free Society, there were no corporate conferences featuring branded lanyards and sponsored lunches. Instead, it was all about challenging the property rights that had granted software companies so much power in the first place. Stallman himself was possibly the movement’s best-known evangelist, traveling around the world to preach about software freedom and the evils of applying patent law to code. - [Freedom isn't free](https://logicmag.io/failure/freedom-isnt-free/)

When I was at college, I would see the [Free Software movement](https://www.gnu.org/philosophy/free-sw.en.html) [0] as the only possible way to ethically produce software. I helped to organize two FliSol in my city. I would help any person that needed help migrating to a Free Software. Everything because I believed in the words of the prophet. I believed software was information and you should never hide information. Imagine, discovering the vaccine for Corona virus and keeping it hidden? No, that sounded horrible.

Of course, during college I would work for "Java shops" (we really called it Software Fabrics) and all the code produced was proprietary [1]. I saw no problem on it, that was how things worked and I needed money to pay the bills. With time, I would also see Google and Apple proprietary software as something they needed (otherwise, how would they profit).

It was in Marx and [Kropotkin](https://thebreadbook.org/) years later that I would re-think my position regarding Free Software. The issue the movement was addressing was not only related with information or speech [2,3,4], but a labour issue. And how do you handle labour issues? By unionizing, working together as a whole. Though, how you discuss labour issues with a group that is famous for thinking individually and relating their success with meritocracy? So I gave up on it. I wasn't going to volunteer for a cause where each energy spent would increase the potential of Big Tech's profit [5]. I just stopped thinking about Free Software, and paused my donations to the Free Software Foundation (FSF).

Working for Big Tech kept my mind busy with other stuff.  It's harder to evaluate the ethics of something when the personal gains are so big. Software Engineers outside the USA do not make fortunes, but for sure much more money than I ever though making my whole life. There were some Open Source things around, and we could give back somethings to the community. But something was missing. A better mission than making a rich person richer.

Even though I was not publicly speaking much about Free Software, deep down I always hopped to work in a place like Mozilla [6], they seemed to be doing some good without exploiting the users or the engineers. But [the big Mozilla's layoff happened](https://www.theverge.com/2020/8/11/21363424/mozilla-layoffs-quarter-staff-250-people-new-revenue-focus) and I didn't know how to digest the news. It seemed like Capitalism had killed Free Software. This was especially bad since the executives from Mozilla were still making a lot of money [7]. Not that reducing their salaries would save the company, but what about a political gesture?

It was literally the end of an era and some people called it on [8,9,10], F/OSS (free and open source software) is dead [11]. A lot of people on the social networks didn't know what would happen next.

Though, the real question is: what should we do? And I really mean we, not the rockstar engineers, you and me, the average engineer. Our king is in check, how we save him and win this game? I don't have a clue, the only thing I know is that we should learn from our mistakes. Expecting capitalist companies to do the right thing is stupid. Acting individually is not enough, acting as a guild is not enough. We need to think as a class [12].

## Notes

[0] - I rather use the term Libre Software, but since I refer a lot to the Free Software Foundation, I decided to use the English word, since Stallman prefer that way.

[1] - Not even the shops owned the code, but the company that hired them to do the job.

[2] - "Coding Freedom: The Ethics and Aesthetics of Hacking" repeats a couple of times that Software would be like Speech and that's why hackers thought it needed to be Libre.

[3] - [The Telekommunisten Manifesto](https://media.telekommunisten.net/manifesto.pdf) goes beyond the "free speech" subject and also address the false information scarcity created by corporations profiting from selling it online.

[4] - Though it's clear that Richard Stallman also thinks Free Software is not only a "Free Speech" movement by his [Personal Declaration in Curitiba - Brasil](https://stallman.org/solidarity-economy.html).

[5] - Not only they were writing Open Source Software, but they were actively addressing the Surveillance Capitalism issues. Plus, they are a not-for-profit organization.

[6] - Trying to increase the adoption of Android does in a way helps spreading open source, but also increases Google's ability to profit.

[7] - [A quote from the CEO](https://answers.thenextweb.com/s/mitchell-baker-aGY62z):
>Executive compensation is a general topic -- are execs, esp CEOs paid too much? I'm of the camp that thinks the different between exec comp and other comp is high. So then i think, OK what should mozilla do about it? My answer is that we try to mitigate this, but we won't solve this general social problem on our own. Here's what I mean by mitigate: we ask our executives to accept a discount from the market-based pay they could get elsewhere. But we don't ask for an 75-80% discount. I use that number because a few years ago when the then-ceo had our compensation structure examined, I learned that my pay was about an 80% discount to market. Meaning that competitive roles elsewhere were paying about 5 times as much. That's too big a discount to ask people and their families to commit to.

[8] - [General Intellect Unit](http://generalintellectunit.net/e/066-post-open-source/) podcast about "Post-Open source world"

[9] - [Melody Horn's text about Post-Open source](https://www.boringcactus.com/2020/08/13/post-open-source.html)

[10] - Well, in theory the Post Open Source Software (POSS) term is [from 2012](https://www.infoworld.com/article/2615869/github-needs-to-take-open-source-seriously.html)

[11] - [Kat Marchán on twitter](https://twitter.com/zkat__/status/1293626135142477825)

[12] - [A Hacker class](https://books.google.ie/books?id=ZuHN7tgkcFIC) and unite with the workers of the world!