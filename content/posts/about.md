---
title: "About"
date: 2020-04-14T21:45:43+01:00
draft: false
---

This is probably my fourth attempt to keep a blog and my first to do it in English. In times where there is so much noise on the internet I really ask myself, what should I add that wasn't said before. And my conclusion is: "nothing".

This will be a weird blog, I will avoid writing on it, I will instead spread as many links here as I can. Instead of writing what was said before, I will point to people that wrote it much better than I could ever do.

The links will be mostly about workplace autonomy, cooperation, and solidarity.

## Myself

I was born in Brazil, I’ve been living in Ireland since 2015. I oppose intellectual property or the commodification of knowledge. You probably will find me either reading (fiction, non-fiction) or hacking something. I do love to hack, I may share some hacks in [GitHub](https://www.github.com/era) or [Gitlab](https://gitlab.com/masterless), although I would like to use more open tools for that. I do not trust corporations, remember when GitHub blocked [Iranian devs](https://techcrunch.com/2019/07/29/github-ban-sanctioned-countries/)? Maybe one day they will do with Brasilians.

I’m a member of the [FSF](https://www.fsf.org/) and [IWW union](https://www.onebigunion.ie/). I support: [FSFE](https://fsfe.org/contribute/contribute.en.html), [Schools for Chiapas](https://schoolsforchiapas.org/donate/), [Cuba Solidarity Campaign](https://cuba-solidarity.org.uk/), [Movimento dos trabalhadores sem teto](https://www.catarse.me/colabore_mtst).

Still curious about me? Check my [personal wiki entry](https://wiki.anarchist.work/personal).

![](https://static.fsf.org/nosvn/associate/crm/642423.png)

